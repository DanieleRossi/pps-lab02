package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractGenerator implements SequenceGenerator {

    protected List<Integer> sequenceOfDigits;
    protected int stop;
    protected int next;

    public AbstractGenerator(int next, int stop)
    {
        this.sequenceOfDigits = new ArrayList<>();
        this.next = next;
        this.stop = stop;
        populateSequenceOfDigits();
    }

    @Override
    public Optional<Integer> next() {
        if(!isOver())
        {
            return Optional.of(sequenceOfDigits.get(next++));
        }
        else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        next = 0;
    }

    @Override
    public boolean isOver() {
        return next == stop;
    }

    @Override
    public List<Integer> allRemaining() {
        return sequenceOfDigits.subList(next,stop);
    }

    protected abstract void populateSequenceOfDigits();
}

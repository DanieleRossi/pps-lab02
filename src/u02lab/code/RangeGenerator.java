package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator extends AbstractGenerator {

    private int start;

    public RangeGenerator(int start, int stop){
        super(0,stop-start+1);
        this.start = start;
    }

    @Override
    protected void populateSequenceOfDigits() {
        sequenceOfDigits = IntStream.rangeClosed(start,stop).boxed().collect(Collectors.toList());
    }
}

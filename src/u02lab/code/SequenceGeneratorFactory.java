package u02lab.code;

public class SequenceGeneratorFactory implements GeneratorFactory {
    @Override
    public RandomGenerator createRandomGenerator(int n) {
        return new RandomGenerator(n);
    }

    @Override
    public RangeGenerator createRangeGenerator(int next, int stop) {
        return new RangeGenerator(next,stop);
    }
}

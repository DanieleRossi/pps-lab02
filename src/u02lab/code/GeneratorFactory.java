package u02lab.code;

public interface GeneratorFactory {
    RandomGenerator createRandomGenerator(int n);
    RangeGenerator createRangeGenerator(int next, int stop);
}

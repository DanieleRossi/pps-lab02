package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RandomGeneratorTest {
    private SequenceGenerator randomGenerator;
    private static final int RANDOM_NORMALIZATION_FACTOR = 10;
    private int numbersOfGenerations;

    @Before
    public void setUp(){
        numbersOfGenerations = (int)(Math.random()*RANDOM_NORMALIZATION_FACTOR)+1;
        randomGenerator = new SequenceGeneratorFactory().createRandomGenerator(numbersOfGenerations);
    }

    @Test
    public void checkFirstBitOfSequence(){
        int nextBit = randomGenerator.next().get();
        assertTrue("The first element of the sequence must by 0.",nextBit == 0 || nextBit == 1);
    }

    @Test
    public void sequenceRestartAfterReset(){
        getGeneratorEnded();
        randomGenerator.reset();
        int nextBit = randomGenerator.next().get();
        assertTrue("After the reset operation the value of next element must be 0 or 1.", nextBit == 0 || nextBit == 1);
    }

    private void getGeneratorEnded(){
        for(int i = 0; i <= numbersOfGenerations; i++){
            randomGenerator.next();
        }
    }

    @Test
    public void sequenceIsNotInitiallyOver(){
        assertFalse("The sequence must not initially over.",randomGenerator.isOver());
    }

    @Test
    public void  sequenceIsEventuallyOver(){
        getGeneratorEnded();
        assertTrue("After generate all sequence the generator must be over.",randomGenerator.isOver());
    }

    @Test
    public void noBitOfSequenceAfterGeneratorIsOver(){
        getGeneratorEnded();
        assertEquals("The generator must not return a number after it is over.", Optional.empty(), randomGenerator.next());
    }

    @Test
    public void canRestartSequenceAfterGeneratorIsOver(){
        getGeneratorEnded();
        randomGenerator.reset();
        int nextBit = randomGenerator.next().get();
        assertTrue("After the reset operation the value of next element must be 0 or 1.", nextBit == 0 || nextBit == 1);
    }
    @Test
    public void allRemainingInitiallyContainsAllNumber(){
        List<Integer> remainingSequence = randomGenerator.allRemaining();
        assertTrue("The remaining bits sequence must contains all numbers initially.", remainingSequence.size() == numbersOfGenerations && remainingSequence.stream().allMatch(b -> b==0 || b==1));
    }
    @Test
    public void allRemaningDoesNotContainFirstElement(){
        randomGenerator.next();
        List<Integer> remainingSequence = randomGenerator.allRemaining();
        assertTrue("The remaining bits sequence must not contains first element.", remainingSequence.size() == numbersOfGenerations-1 && remainingSequence.stream().allMatch(b -> b==0 || b==1));
    }
    @Test
    public void allRemainingIsEmptyAfterGeneratorIsOver(){
        getGeneratorEnded();
        assertEquals("The remaining bits sequence must be empty after generator is over.", new ArrayList<Integer>(), randomGenerator.allRemaining());
    }

    @Test
    public void  allRemainingContainsAllNumbersAfterReset(){
        getGeneratorEnded();
        randomGenerator.reset();
        List<Integer> remainingSequence = randomGenerator.allRemaining();
        assertTrue("The remaining bits sequence must contains all numbers after reset.", remainingSequence.size() == numbersOfGenerations && remainingSequence.stream().allMatch(b -> b==0 || b==1));
    }
}

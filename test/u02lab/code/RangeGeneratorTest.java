package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.*;

import static org.junit.Assert.*;


public class RangeGeneratorTest {
    private SequenceGenerator rangeGenerator;
    private static final int START_NUMBER = 0;
    private static final int RANDOM_NORMALIZATION_FACTOR = 10;
    private int stopNumber;

    @Before
    public void setUp(){
        stopNumber = (int)(Math.random()*RANDOM_NORMALIZATION_FACTOR);
        System.out.print(stopNumber);
        rangeGenerator = new SequenceGeneratorFactory().createRangeGenerator(START_NUMBER, stopNumber);
    }

    @Test
    public void checkFirstNumberOfSequence(){
        assertEquals("The first element of the sequence must by 0.", Optional.of(START_NUMBER),rangeGenerator.next());
    }

    @Test
    public void checkAllNumberOfSequence(){
        assertEquals("The obtained sequence must be equals to the expected one.",getSequenceFromEnds(START_NUMBER,stopNumber),getAllNumberOfSequence());
    }

    private List<Integer> getSequenceFromEnds(int start, int stop)
    {
        return IntStream.rangeClosed(start,stop).boxed().collect(Collectors.toList());
    }

    @Test
    public void sequenceRestartAfterReset(){
        rangeGenerator.next();
        rangeGenerator.reset();
        assertEquals("After the reset operation the value of next element must be 0.", Optional.of(START_NUMBER), rangeGenerator.next());
    }

    @Test
    public void sequenceIsNotInitiallyOver(){
        assertFalse("The sequence must not initially over.",rangeGenerator.isOver());
    }

    private List<Integer> getAllNumberOfSequence(){
        List<Integer> sequenceNumbers = new ArrayList<>();
        for (int i = 0; i <= stopNumber - START_NUMBER; i++){
            sequenceNumbers.add(rangeGenerator.next().get());
        }
        return sequenceNumbers;
    }

    @Test
    public void  sequenceIsEventuallyOver(){
        getAllNumberOfSequence();
        assertTrue("After generate all sequence the generator must be over.",rangeGenerator.isOver());
    }

    @Test
    public void noNumberOfSequenceAfterGeneratorIsOver(){
        getAllNumberOfSequence();
        assertEquals("The generator must not return a number after it is over.", Optional.empty(),rangeGenerator.next());
    }

    @Test
    public void canRestartSequenceAfterGeneratorIsOver(){
        getAllNumberOfSequence();
        rangeGenerator.reset();
        assertEquals("After the reset operation the value of next element must be 0.",Optional.of(START_NUMBER),rangeGenerator.next());
    }

    @Test
    public void allRemainingInitiallyContainsAllNumber(){
        assertEquals("The remaining numbers sequence must contains all numbers initially.", getSequenceFromEnds(START_NUMBER,stopNumber),rangeGenerator.allRemaining());
    }

    @Test
    public void allRemaningDoesNotContainFirstElement(){
        rangeGenerator.next();
        assertEquals("The remaining numbers sequence must not contains the first element.", getSequenceFromEnds(START_NUMBER+1,stopNumber), rangeGenerator.allRemaining());
    }

    @Test
    public void allRemainingIsEmptyAfterGeneratorIsOver(){
        getAllNumberOfSequence();
        assertEquals("The remaining numbers sequence must be empty after generator is over.", new ArrayList<Integer>(), rangeGenerator.allRemaining());
    }

    @Test
    public void  allRemainingContainsAllNumbersAfterReset(){
        getAllNumberOfSequence();
        rangeGenerator.reset();
        assertEquals("The remaining numbers sequence must contains all numbers after reset.", getSequenceFromEnds(START_NUMBER,stopNumber), rangeGenerator.allRemaining());
    }
}
